<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthController
{
    private UserRepository $repository;

    public function __construct()
    {
        $this->repository = new UserRepository();
    }

    /**
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $user = (new UserRepository())->getUserByEmail($request->get('email'));

        if (!$user){
            throw new HttpException(JsonResponse::HTTP_FORBIDDEN,'не верный логин');
        }
        if (!Hash::check($request->get('password'),$user->password)) {
            throw new HttpException(JsonResponse::HTTP_FORBIDDEN,'не верный пароль');
        }

        $token = $this->repository->setAuthToken($user->id);
        return response()->json([
            'token' => $token
        ]);
    }

}
