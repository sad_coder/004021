<?php

namespace App\Http\Controllers;

use App\Repositories\BookRepository;

class BookController extends Controller
{

    public function __construct()
    {
        $this->repository = new BookRepository();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() : \Illuminate\Http\JsonResponse
    {
        return response()->json($this->repository->getList()->toArray());
    }
}
