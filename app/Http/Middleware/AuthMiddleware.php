<?php

namespace App\Http\Middleware;

use App\Services\AuthService;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return false
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->hasHeader('Authorization')){
            throw new HttpException(Response::HTTP_FORBIDDEN,'нет доступа');
        }

        if (!AuthService::checkAuth($request->header('Authorization'))){
            throw new HttpException(Response::HTTP_FORBIDDEN,'нет доступа');
        }
        return $next($request);
    }
}
