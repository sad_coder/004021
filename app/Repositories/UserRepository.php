<?php

namespace App\Repositories;

use App\Models\User as Model;
use App\Services\AuthService;

class UserRepository extends CoreRepository
{
    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }

    /**
     * @param string $email
     * @param string $password
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getUserByEmail(string $email)
    {
        return ($this->startCondition())::query()
            ->where(['email' => $email])
            ->first();
    }

    /**
     * @param string $token
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getUserByToken(string $token)
    {
        return $this->startCondition()::query()
            ->where(['auth_token' => $token])
            ->first();
    }

    /**
     * @param int $id
     * @param string $token
     */
    public function setAuthToken(int $id): string
    {
        $token = AuthService::generateToken();
        $this->startCondition()::query()
            ->where(['id' => $id])
            ->update(['auth_token' => $token]);
        AuthService::cacheToken($id,$token);
        return $token;
    }

}
