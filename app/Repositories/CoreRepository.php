<?php

namespace App\Repositories;
use Illuminate\Database\Eloquent\Model;


abstract class CoreRepository
{
    /**
     * @var \Illuminate\Database\Eloquent\Model|mixed
     */
    protected $model;

    public function __construct()
    {
        $this->model = app($this->getModelClass());
    }

    /**
     * @return string
     */
    abstract protected function getModelClass() :string;

    /**
     * @return \Illuminate\Contracts\Foundation\Application|Model|mixed
     */
    protected function startCondition()
    {
        return clone $this->model;
    }

    public function massInsert(array $data): void
    {
        ($this->startCondition())::insert($data);
    }

    /**
     * @return Model|mixed
     */
    public function getAll()
    {
        return ($this->startCondition())::all();
    }

}
