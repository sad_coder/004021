<?php

namespace App\Repositories;

use App\Filters\CategoryTagFilter;
use App\Models\Category as Model;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CategoryRepository extends CoreRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Collection|QueryBuilder[]
     */
    public function getList()
    {
        return QueryBuilder::for($this->getModelClass())
            ->with('books')
            ->allowedSorts('id')
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::exact('books.id'),
                'books.name',
                'name',
                AllowedFilter::custom('tag', new CategoryTagFilter),
            ])
            ->get();
    }

    protected function getModelClass(): string
    {
        return Model::class;
    }
}
