<?php

namespace App\Repositories;

use App\Filters\BookTagFilter;
use App\Models\Book as Model;
use Illuminate\Database\Eloquent\Collection;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class BookRepository extends CoreRepository
{

    /**
     * @return \Illuminate\Database\Eloquent\Collection|QueryBuilder[]
     */
    public function getList() :Collection
    {
        return QueryBuilder::for($this->getModelClass())
            ->with('categories')
            ->allowedSorts('id')
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::exact('categories.id'),
                'categories.name',
                'name',
                AllowedFilter::custom('tag', new BookTagFilter()),
            ])
            ->get();
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}
