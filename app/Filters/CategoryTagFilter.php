<?php

namespace App\Filters;
use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class CategoryTagFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $query->whereHas('books', function (Builder $query) use ($value) {
            $query->where('tag', $value);
        })->orWhereHas('categories',function (Builder $query) use ($value) {
            $query->where('tag', $value);
        });
    }
}
