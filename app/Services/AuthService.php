<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class AuthService
{
    private const CACHED_TOKEN_TIME = 86400 * 30;

    public static function checkAuth(string $token)
    {
        return self::checkTokenInCache($token) ?? (new UserRepository())->getUserByToken($token);
    }

    /**
     * @param int $userId
     * @param $token
     */
    public static function cacheToken(int $userId, string $token): void
    {
        Cache::put("token:$token",$userId,self::CACHED_TOKEN_TIME);
    }

    public static function checkTokenInCache(string $token)
    {
       return Cache::get("token:$token");
    }

    /**
     * @return string
     */
    public static function generateToken(): string
    {
        return Str::random(255);
    }

}
