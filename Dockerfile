FROM ubuntu:18.04

RUN cat /etc/os-release

ENV TZ=Asia/Almaty
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install software-properties-common -y && add-apt-repository ppa:ondrej/php && apt update && apt install php7.4 -y
RUN apt-get update --fix-missing -y && apt-get install composer  php7.4-cli php7.4-json php7.4-common php7.4-zip php7.4-gd php7.4-mbstring php7.4-curl php7.4 php7.4-bcmath php7.4-pdo php7.4-fpm php7.4-curl php7.4-xml php7.4-pgsql nginx postgresql-client php7.4-redis redis-tools -y
RUN apt-get install -y locales
RUN apt-get install -y supervisor
RUN apt-get install -y htop

#memcahed
RUN apt-get install -y php7.4-memcached

#memcached

RUN locale-gen ru_RU.UTF-8 && update-locale LC_ALL=ru_RU.UTF-8 LANG=ru_RU.UTF-8

RUN rm -Rf /etc/nginx/sites-enabled/*.*
RUN unlink /etc/nginx/sites-enabled/default
COPY build-local/nginx/service.conf /etc/nginx/sites-enabled/service.conf

COPY build-local/supervisord.conf /etc/supervisor/conf.d/

RUN service php7.4-fpm start
RUN service nginx start

WORKDIR /var/www/abelohost-service

EXPOSE 8052

CMD ["/usr/bin/supervisord"]
