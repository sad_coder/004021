<?php

namespace Database\Seeders;

use App\Repositories\BookRepository;
use App\Repositories\CategoryRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class AttachBooksToCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books = (new BookRepository())->getAll();
        $categories = (new CategoryRepository())->getAll();
        $defaultTags = [
            '#приключение',
            '#хештег',
            '#тегДляПримера'
        ];
        foreach($books as $book){
            $book->categories()->attach([
                $categories->random(1)->first()->id => ['tag' => Arr::random($defaultTags)],
            ]);
        }

    }
}
