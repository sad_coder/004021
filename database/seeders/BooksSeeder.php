<?php

namespace Database\Seeders;

use App\Repositories\BookRepository;
use Illuminate\Database\Seeder;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultBooks = [
            [
                'name' => 'Гарри Поттер',
                'description' => 'Описание книги по Гарри Поттеру'
            ],
            [
                'name' => 'Мартин Иден',
                'description' => 'Описание книги Мартин Иден'
            ],
            [
                'name' => 'Паттерны проектирования на PHP',
                'description' => 'Описание для книги Паттерны проектирования на PHP'
            ],
            [
                'name' => '7 баз данных за 7 недель',
                'description' => 'Описание для книги 7 баз данных за 7 недель'
            ],
        ];
        (new BookRepository())->massInsert($defaultBooks);
    }
}
