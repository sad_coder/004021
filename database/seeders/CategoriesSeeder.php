<?php

namespace Database\Seeders;

use App\Repositories\CategoryRepository;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultCategories = [
            [
                'name' => 'Приключение',
                'description' => 'Описание жанра приключение'
            ],
            [
                'name' => 'Детектив',
                'description' => 'Описание жанра детектив'
            ],
            [
                'name' => 'Роман',
                'description' => 'Описание жанра роман'
            ],
            [
                'name' => 'Нон-фикшн',
                'description' => 'Описание жанра нон-фикшн'
            ],
        ];
        (new CategoryRepository())->massInsert($defaultCategories);
    }
}
